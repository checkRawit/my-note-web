#!/usr/bin/env sh
docker run --rm -it \
    -p 3000:3000 \
    --mount type=bind,source="$(pwd)",target=/app \
    --workdir /app \
    -u $(id -u ${USER}):$(id -g ${USER}) \
    node:14-alpine /bin/sh