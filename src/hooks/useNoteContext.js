import { useContext } from "react";

import { NotesContext } from "../context/NoteProvider";
import {
  deleteNote as deleteNoteAPI,
  postNote as postNoteAPI,
  setNoteArchive as setNoteArchiveAPI,
  editNote as editNoteAPI,
} from "../services/note";

export default () => {
  const { notesState, notesDispatch } = useContext(NotesContext);

  const deleteNote = async (id) => {
    const isResponseOK = await deleteNoteAPI(id);
    if (!isResponseOK) return;
    notesDispatch({ type: "NOTES_CHANGED" });
  };

  const postNote = async (message) => {
    const isResponseOK = await postNoteAPI(message);
    if (!isResponseOK) return; // TODO: unsuccessful request
    notesDispatch({ type: "NOTES_CHANGED" });
  };

  const setNotesType = (type) => {
    notesDispatch({ type: "SET_TYPE", payload: { type } });
  };

  const setNoteArchive = async (noteId, isArchived) => {
    const isResponseOK = await setNoteArchiveAPI(noteId, isArchived);
    if (!isResponseOK) return; // TODO: unsuccessful request
    notesDispatch({ type: "NOTES_CHANGED" });
  };

  const editNote = async (noteId, message) => {
    const isResponseOK = await editNoteAPI(noteId, message);
    if (!isResponseOK) return false;
    notesDispatch({ type: "NOTES_CHANGED" });
    return true;
  };

  const setNotesTag = async (tag) => {
    notesDispatch({ type: "SET_TAG", payload: { tag } })
  }

  return {
    notes: notesState.notes,
    notesType: notesState.type,
    notesTag: notesState.tag,
    noteTags: notesState.tags,
    notesNeedRefresh: notesState.needRefresh,
    noteTagsNeedRefresh: notesState.needRefreshTags,
    deleteNote,
    postNote,
    setNotesType,
    setNotesTag,
    setNoteArchive,
    editNote,
    notesDispatch
  };
};
