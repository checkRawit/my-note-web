import { StrictMode } from "react";
import ReactDOM from "react-dom";
import { UserProvider } from "./context/UserProvider";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(
  <StrictMode>
    <UserProvider>
      <App />
    </UserProvider>
  </StrictMode>,
  document.getElementById("app")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below.
serviceWorker.unregister();
