import { useEffect } from "react";
import { useState } from "react";
import { useContext } from "react";
import { UserContext } from "../../context/UserProvider";
import { changeDisplayName, changePassword } from "../../services/user";
import classNames from "classnames";

import ImageTextSection from "../../components/ImageTextSection";

import SignInLogo from "./assets/undraw_sign_in.svg";

/*
TODO:
- ✅ Set displayName 
- ❔ Display displayName
- Change password
- Show email
*/

const useEditName = () => {
  const { userState, userDispatch } = useContext(UserContext);
  const [displayName, setDisplayName] = useState(userState.displayName || "");
  const [isSubmiting, setIsSubmiting] = useState(false);

  useEffect(() => {
    setDisplayName(userState.displayName || "");
  }, [userState.displayName]);

  const onSubmit = async () => {
    setIsSubmiting(true);
    try {
      await changeDisplayName(displayName);
      userDispatch({
        type: "SET_DISPLAY_NAME",
        payload: {
          displayName: displayName,
        },
      });
    } finally {
      setIsSubmiting(false);
    }
  };

  return { displayName, setDisplayName, onSubmit, isSubmiting };
};

const useEditPassword = () => {
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [isSubmiting, setIsSubmiting] = useState(false);
  const [isError, setIsError] = useState(false);

  const onSubmit = async () => {
    try {
      setIsError(false);
      setIsSubmiting(true);
      await changePassword(oldPassword, newPassword);
    } catch (error) {
      setIsError(true);
    } finally {
      setIsSubmiting(false);
    }
  };

  return {
    onSubmit,
    oldPassword,
    newPassword,
    isSubmiting,
    setOldPassword,
    setNewPassword,
    isError,
  };
};

const UserPage = (props) => {
  const { userState } = useContext(UserContext);
  const { displayName, setDisplayName, onSubmit, isSubmiting } = useEditName();
  const {
    oldPassword,
    newPassword,
    setOldPassword,
    setNewPassword,
    isSubmiting: isSubmitingPasswordChange,
    onSubmit: onSubmitPasswordChange,
    isError: isErrorPasswordChange,
  } = useEditPassword();

  // Tell user to login
  if (!userState.uid) {
    return (
      <ImageTextSection
        imageSrc={SignInLogo}
        imageAlt="signin"
        title="User login is required"
        subtitle="Please login to continue"
        isFullHeight
      />
    );
  }

  // Default
  return (
    <section className="section">
      <div className="container">
        {/* Basic info sectiion */}
        <div className="card">
          <div className="card-header">
            <p className="card-header-title">Edit</p>
          </div>
          <div className="card-content">
            <fieldset disabled={isSubmiting}>
              <div className="field">
                <label className="label">Display name</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    value={displayName}
                    onChange={(e) => setDisplayName(e.target.value)}
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <button
                    className={classNames({
                      "button is-success": true,
                      "is-loading": isSubmiting,
                    })}
                    onClick={onSubmit}
                  >
                    Update
                  </button>
                </div>
              </div>
            </fieldset>
          </div>
        </div>

        {/* Change password section */}
        <div className="card mt-6">
          <div className="card-header">
            <p className="card-header-title">Change the password</p>
          </div>
          <div className="card-content">
            <div
              className={classNames({
                "notification is-danger": true,
                "is-hidden": !isErrorPasswordChange,
              })}
            >
              An Error occurred while attempting to change the password.
            </div>
            <fieldset disabled={isSubmitingPasswordChange}>
              <div className="field">
                <label className="label">Old password</label>
                <div className="control">
                  <input
                    type="password"
                    className="input"
                    value={oldPassword}
                    onChange={(e) => setOldPassword(e.target.value)}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">New password</label>
                <div className="control">
                  <input
                    type="password"
                    className="input"
                    value={newPassword}
                    onChange={(e) => setNewPassword(e.target.value)}
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <button
                    className={classNames({
                      "button is-danger": true,
                      "is-loading": isSubmitingPasswordChange,
                    })}
                    onClick={onSubmitPasswordChange}
                  >
                    Change
                  </button>
                </div>
              </div>
            </fieldset>
          </div>
        </div>
      </div>
    </section>
  );
};

export default UserPage;
