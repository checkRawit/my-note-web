import { useState } from "react";
import classNames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCircleNotch,
  faTimes,
  faUser,
  faArchive,
  faClock,
  faPen,
} from "@fortawesome/free-solid-svg-icons";
import ReactMarkdown from "react-markdown";
import remarkGfm from "remark-gfm";

const Note = (props) => {
  const { children, footer, title, levelRightItems } = props;

  return (
    <div className={classNames("box")}>
      {/* Header */}
      <div className="content">
        <nav className="level is-mobile">
          <div className="level-left">
            <div className="level-item">
              <span className="icon is-small has-text-info">
                <FontAwesomeIcon icon={faUser} />
              </span>
            </div>
            <div className="level-item">
              <strong>{title}</strong>
            </div>
          </div>
          {levelRightItems && (
            <div className={classNames("level-right")}>
              {levelRightItems.map((element, index) => (
                <div key={index} className="level-item">
                  {element}
                </div>
              ))}
            </div>
          )}
        </nav>
      </div>
      {/* Body */}
      <div className="content">{children}</div>
      {/* Footer */}
      {footer}
    </div>
  );
};

const useWaitForHandler = (handler) => {
  const [isWaiting, setIsWaiting] = useState(false);

  const submitHandler = async (...args) => {
    setIsWaiting(true);
    await handler(...args);
    setIsWaiting(false);
  };

  return [isWaiting, submitHandler];
};

export function NoteForm(props) {
  const {
    onClickNoteButton,
    title,
    onClickCloseButton,
    initialMessage = "",
  } = props;

  const [message, setMessage] = useState(initialMessage);
  const [isNoteSubmitting, noteSubmitHandler] = useWaitForHandler(
    onClickNoteButton
  );

  return (
    <Note
      title={title}
      levelRightItems={[
        <span
          className="icon is-small"
          style={{ cursor: "pointer" }}
          onClick={onClickCloseButton}
        >
          <FontAwesomeIcon icon={faTimes} />
        </span>,
      ]}
      footer={
        <button
          className={classNames({
            button: true,
            "is-success": true,
            "is-loading": isNoteSubmitting,
          })}
          onClick={async () => {
            await noteSubmitHandler(message);
            // WARNING: Auto click close button after submiting
            onClickCloseButton()
          }}
          disabled={isNoteSubmitting}
        >
          Note
        </button>
      }
    >
      <textarea
        className="textarea"
        placeholder="e.g. Hello world"
        value={message}
        onChange={(event) => setMessage(event.target.value)}
        rows={2}
      ></textarea>
    </Note>
  );
}

export function NoteDisplay(props) {
  const {
    message,
    date,
    title,
    onClickDelete,
    onClickArchive,
    onClickEdit,
  } = props;

  const [isDeleting, deleteHandler] = useWaitForHandler(onClickDelete);
  const [isArchiving, archiveHandler] = useWaitForHandler(onClickArchive);

  const [showEditForm, setShowEditForm] = useState(false);

  // show Note Form instead if state is true
  if (showEditForm) {
    return (
      <NoteForm
        title={title}
        onClickNoteButton={async (message) => {
          await onClickEdit(message);
          setShowEditForm(false);
        }}
        onClickCloseButton={() => setShowEditForm(false)}
        initialMessage={message}
      />
    );
  }

  /* Item in level-right */
  const levelRightItems = [];

  // add archive icon if handler is exist
  onClickArchive &&
    levelRightItems.push(
      <span
        className="icon is-small"
        style={{ cursor: "pointer" }}
        onClick={archiveHandler}
      >
        <FontAwesomeIcon
          icon={!isArchiving ? faArchive : faCircleNotch}
          spin={isArchiving}
        />
      </span>
    );

  // add edit button if onClickEdit is exist
  onClickEdit &&
    levelRightItems.push(
      <span
        className="icon is-small"
        style={{ cursor: "pointer" }}
        onClick={() => setShowEditForm(true)}
      >
        <FontAwesomeIcon icon={faPen} />
      </span>
    );

  // add delete icon if handler is exist
  onClickDelete &&
    levelRightItems.push(
      <span
        className="icon is-small has-text-danger"
        style={{ cursor: "pointer" }}
        onClick={deleteHandler}
      >
        <FontAwesomeIcon
          icon={!isDeleting ? faTimes : faCircleNotch}
          spin={isDeleting}
        />
      </span>
    );

  return (
    <Note
      title={title}
      levelRightItems={levelRightItems}
      footer={
        <nav className="level is-mobile is-clipped">
          <div className="level-left">
            <div className="level-item">
              <span className="icon is-small">
                <FontAwesomeIcon icon={faClock} />
              </span>
            </div>
            <div className="level-item">
              <small>{date.toLocaleString()}</small>
            </div>
          </div>
        </nav>
      }
    >
      <ReactMarkdown remarkPlugins={[remarkGfm]} children={message} />
    </Note>
  );
}
