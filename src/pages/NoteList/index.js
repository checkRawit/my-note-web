import { useContext, useEffect, useState } from "react";
import classNames from "classnames";
import { Link, useLocation } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlus,
  faClipboardList,
  faArchive,
  faAngleDown,
} from "@fortawesome/free-solid-svg-icons";

import { fetchNotes as fetchNotesAPI, fetchNoteTags as fetchNoteTagsAPI } from "../../services/note";
import { UserContext } from "../../context/UserProvider";
import { NotesProvider } from "../../context/NoteProvider";
import useNotesContext from "../../hooks/useNoteContext";
import ImageTextSection from "../../components/ImageTextSection";
import { NoteDisplay, NoteForm } from "./components/Note";
import { opacity } from "./styles.module.css";
import SignInLogo from "./assets/undraw_sign_in.svg";
import NoDataLogo from "./assets/undraw_no_data.svg";

const NotesSection = (props) => {
  const { userState } = useContext(UserContext);
  const {
    notes,
    notesType,
    notesTag,
    noteTags,
    notesNeedRefresh,
    noteTagsNeedRefresh,
    deleteNote,
    postNote,
    setNotesType,
    setNotesTag,
    setNoteArchive,
    editNote,
    notesDispatch,
  } = useNotesContext();

  /* fetch notes if it need to refresh*/
  useEffect(() => {
    const fetchNotes = async () => {
      notesDispatch({
        type: "NOTES_REFRESHED",
        payload: await fetchNotesAPI(notesType, notesTag),
      });
    };
    notesNeedRefresh && fetchNotes()
  }, [notesNeedRefresh]);

  useEffect(() => {
    notesDispatch({
      type: "NOTES_CHANGED"
    })
  }, [userState.uid])

  useEffect(() => {
    const fetchNoteTags = async () => {
      notesDispatch({
        type: "TAGS_REFERSHED",
        payload: { tags: await fetchNoteTagsAPI(notesType) }
      })
    }
    noteTagsNeedRefresh && fetchNoteTags()
  }, [noteTagsNeedRefresh])

  useEffect(() => {
    switch (props.isArchived) {
      case true:
        setNotesType("ARCHIVE");
        break;
      case false:
        setNotesType("DEFAULT");
        break;
      default:
        setNotesType("ALL");
    }
  }, [props.isArchived]);

  useEffect(() => {
    setNotesTag(props.tag)
  }, [props.tag])

  const [showNoteForm, setShowNoteForm] = useState(false);

  const noteComponents = notes.map((note) => {
    const isOwner = userState.uid === note.user_uid;

    return (
      <div className="column is-half-tablet is-one-third-desktop" key={note.id}>
        <NoteDisplay
          message={note.message}
          title={userState.displayName}
          date={new Date(note.create_time_epoch)}
          onClickDelete={isOwner ? () => deleteNote(note.id) : null}
          onClickArchive={isOwner ? () => setNoteArchive(note.id, !note.is_archived) : null}
          onClickEdit={isOwner ? (message) => editNote(note.id, message) : null}
        />
      </div>
    );
  });

  return (
    <>
      {/* Loading bar */}
      <progress
        className="progress is-small is-primary"
        style={{
          position: "absolute",
          display: notesNeedRefresh ? "block" : "none",
        }}
        max="100"
      >
        {" "}
        Loading...{" "}
      </progress>
      {/* Section */}
      <section className="section">
        <div className="container">
          <nav className="level">
            {/* View option */}
            <div className="level-left">
              {/* Notes Type */}
              <div className="level-item">
                <div className="tabs is-toggle is-fullwidth" style={{ flexBasis: "100%" }}>
                  <ul>
                    <li
                      className={classNames({
                        "has-background-white": true,
                        "is-active": notesType === "DEFAULT",
                      })}
                    >
                      <Link to="?isArchived=false">
                        <span className="icon is-small">
                          <FontAwesomeIcon icon={faClipboardList} />
                        </span>
                        <span>Default</span>
                      </Link>
                    </li>
                    <li
                      className={classNames({
                        "has-background-white": true,
                        "is-active": notesType === "ARCHIVE",
                      })}
                    >
                      <Link to="?isArchived=true">
                        <span className="icon is-small">
                          <FontAwesomeIcon icon={faArchive} />
                        </span>
                        <span>Archive</span>
                      </Link>
                    </li>
                    <li
                      className={classNames({
                        "has-background-white": true,
                        "is-active": notesType === "ALL",
                      })}
                    >
                      <Link to="?">All</Link>
                    </li>
                  </ul>
                </div>
              </div>

              {/* Notes Tags */}
              <div className="level-item dropdown is-hoverable">
                <div className="dropdown-trigger" style={{ flexBasis: "100%" }}>
                  <button className="button is-fullwidth" aria-haspopup="true" aria-controls="dropdown-menu">
                    <span>{notesTag != null ? notesTag : "Tags"}</span>
                    <span className="icon is-small">
                      <FontAwesomeIcon icon={faAngleDown} />
                    </span>
                  </button>
                </div>
                <div className="dropdown-menu" id="dropdown-menu" role="menu" style={{ width: "100%" }}>
                  <div className="dropdown-content">
                    <Link to={bulidURL({ notesType })} className={classNames({
                        "dropdown-item": true,
                        "is-active": notesTag === null
                      })}>
                      None
                    </Link>
                    {noteTags.length > 0 && <hr className="dropdown-divider" />}
                    {noteTags.map(t => (
                      <Link key={t} to={bulidURL({ notesType, notesTag: t })} className={classNames({
                        "dropdown-item": true,
                        "is-active": t === notesTag
                      })}>
                        {t}
                      </Link>
                    ))}
                  </div>
                </div>
              </div>

            </div>
            <div className="level-right">
              {/* Add button */}
              <div className="level-item">
                <button
                  className={classNames({
                    button: true,
                    "is-primary": true,
                    "is-fullwidth": true,
                    "is-inverted": !showNoteForm,
                  })}
                  onClick={() => setShowNoteForm(!showNoteForm)}
                >
                  <span className="icon">
                    <FontAwesomeIcon icon={faPlus} />
                  </span>
                  <span>Add</span>
                </button>
              </div>
            </div>
          </nav>
          {/* NoteEdit */}
          {showNoteForm && (
            <NoteForm
              title={userState.displayName}
              onClickNoteButton={postNote}
              onClickCloseButton={() => setShowNoteForm(false)}
            />
          )}
          {/* Note Columns */}
          <div
            className={classNames({
              columns: true,
              "is-multiline": true,
              [opacity]: notesNeedRefresh,
            })}
          >
            {noteComponents}
          </div>
        </div>
      </section>
      {/* Empty note */}
      {!notesNeedRefresh && notes.length === 0 && (
        <ImageTextSection
          imageSrc={NoDataLogo}
          imageAlt="signin"
          title="You have no note"
          subtitle="Add your note here"
        />
      )}
    </>
  );
};

const NoteListPage = (props) => {
  const { userState } = useContext(UserContext);
  const query = new URLSearchParams(useLocation().search);
  const isArchived = getBoolean(query.get("isArchived"));
  const tag = query.get("tag")

  // Show sign in is required
  if (!userState.uid) {
    return (
      <ImageTextSection
        imageSrc={SignInLogo}
        imageAlt="signin"
        title="User login is required"
        subtitle="Please login to continue"
        isFullHeight
      />
    );
  }

  return (
    <NotesProvider>
      <NotesSection isArchived={isArchived} tag={tag} />
    </NotesProvider>
  );
};

export default NoteListPage;

const getBoolean = (value) => {
  switch (value) {
    case "true":
      return true;
    case "false":
      return false;
    default:
      return null;
  }
};

const bulidURL = (params) => {
  const searchParams = new URLSearchParams()

  if (params === undefined) {
    return `?${searchParams.toString()}`
  }

  const { notesType, notesTag } = params

  switch (notesType) {
    case "DEFAULT":
      searchParams.append('isArchived', 'false')
      break
    case "ARCHIVE":
      searchParams.append('isArchived', 'true')
      break
    default:
  }

  if (notesTag !== undefined) {
    searchParams.append('tag', notesTag)
  }

  return `?${searchParams.toString()}`
}