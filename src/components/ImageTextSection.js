import classNames from "classnames";

const ImageTextSection = ({
  imageSrc,
  imageAlt,
  title,
  subtitle,
  isFullHeight = false,
}) => {
  return (
    <section
      className={classNames({
        hero: true,
        "is-fullheight-with-navbar": isFullHeight,
      })}
    >
      <div className="hero-body">
        <div className="container has-text-centered">
          <img
            src={imageSrc}
            alt={imageAlt}
            className="my-5"
            style={{ maxWidth: "28em", maxHeight: "16em", width: "100%" }}
          />
          <h1 className="title">{title}</h1>
          <h2 className="subtitle">{subtitle}</h2>
        </div>
      </div>
    </section>
  );
};

export default ImageTextSection;
