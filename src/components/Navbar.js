import { useState, useContext } from "react";
import classNames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser, faKey } from "@fortawesome/free-solid-svg-icons";

import { UserContext } from "../context/UserProvider";

import { login, logout } from "../services/user";
import { Link } from "react-router-dom";

const UserNavbarItem = ({ displayName, onClickLogout }) => {
  return (
    <>
      <div className="navbar-item">
        <Link to="/me">
          <span className="icon has-text-info">
            <FontAwesomeIcon icon={faUser} />
          </span>
          <span>{displayName}</span>
        </Link>
      </div>
      <div className="navbar-item">
        <button
          className="button is-fullwidth is-small is-danger"
          onClick={onClickLogout}
        >
          Log out
        </button>
      </div>
    </>
  );
};

// Login logic
const useLogin = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoginError, setIsLoginError] = useState(false);

  const onLogin = () => {
    setIsLoginError(false);
    login(username, password).catch((error) => {
      setIsLoginError(true);
      console.error(error.code, error.message);
    });
  };

  return { setUsername, setPassword, isLoginError, onLogin };
};

const LoginFormNavbarItem = (props) => {
  const { onUsernameChange, onPasswordChange, isError, onClickLogIn } = props;
  return (
    <>
      <div className="navbar-item field-horizontal">
        <div className="field-body">
          <div className="field">
            <div className="control is-expanded has-icons-left">
              <input
                type="text"
                className={classNames({
                  input: true,
                  "is-small": true,
                  "is-danger": isError,
                })}
                placeholder="Username"
                onChange={(event) => onUsernameChange(event.target.value)}
              />
              <span className="icon is-small is-left">
                <FontAwesomeIcon icon={faUser} />
              </span>
            </div>
          </div>
          <div className="field">
            <div className="control is-expanded has-icons-left">
              <input
                type="password"
                className={classNames({
                  input: true,
                  "is-small": true,
                  "is-danger": isError,
                })}
                placeholder="Password"
                onChange={(event) => onPasswordChange(event.target.value)}
              />
              <span className="icon is-small is-left">
                <FontAwesomeIcon icon={faKey} />
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="navbar-item">
        <div className="control is-expanded">
          <button
            className="button is-fullwidth is-small is-primary"
            onClick={onClickLogIn}
          >
            Log in
          </button>
        </div>
      </div>
    </>
  );
};

const Navbar = () => {
  const { userState } = useContext(UserContext);
  const [isMenuActive, setIsMenuActive] = useState(false);

  const { setUsername, setPassword, isLoginError, onLogin } = useLogin();

  return (
    <div className="has-background-white">
      <div className="container">
        <nav className="navbar">
          <div className="navbar-brand">
            <div className="navbar-item">
              <Link to="/">
                <h1 className="title is-4 has-text-weight-bold">My-Notes</h1>
              </Link>
            </div>
            <button
              className="navbar-burger button is-white"
              data-target="navMenu"
              aria-label="menu"
              aria-expanded="false"
              onClick={() => setIsMenuActive(!isMenuActive)}
            >
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </button>
          </div>
          <div
            className={classNames({
              "navbar-menu": true,
              "is-active": isMenuActive,
            })}
            id="navMenu"
          >
            <div className="navbar-end">
              {/* Check if user is login */}
              {userState.uid ? (
                /* User info */
                <UserNavbarItem
                  displayName={userState.displayName}
                  onClickLogout={logout}
                />
              ) : (
                /* Login Form */
                <LoginFormNavbarItem
                  onUsernameChange={setUsername}
                  onPasswordChange={setPassword}
                  isError={isLoginError}
                  onClickLogIn={onLogin}
                />
              )}
            </div>
          </div>
        </nav>
      </div>
    </div>
  );
};

export default Navbar