import { useEffect, useContext } from "react";
import "./App.scss";
import firebase from "./services/firebase";
import { getAuth, onAuthStateChanged } from 'firebase/auth'
import { UserContext } from "./context/UserProvider";
import NoteListPage from "./pages/NoteList";
import UserPage from "./pages/User";
import Navbar from "./components/Navbar";

import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";

function App() {
  const { userDispatch } = useContext(UserContext);

  useEffect(() => {
    // Auth status handling
    return onAuthStateChanged(getAuth(firebase), (user) => {
      if (user) {
        console.log("User is signed in");
        userDispatch({
          type: "LOGIN",
          payload: {
            displayName: user.displayName,
            user: user.uid,
          },
        });
      } else {
        userDispatch({ type: "LOGOUT" });
        console.log("No user is signed in.");
      }
    });
  }, []);

  return (
    <Router>
      <Navbar />
      <Switch>
        <Route path="/me">
          <UserPage />
        </Route>
        <Route path="/notes">
          <NoteListPage />;
        </Route>
        <Redirect to="/notes?isArchived=false" />
      </Switch>
    </Router>
  );
}

export default App;
