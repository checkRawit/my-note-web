import { createContext, useReducer } from "react";

const notesInitialState = {
  notes: [],
  type: "DEFAULT",
  tag: null,
  tags: [],
  needRefresh: true,
  needRefreshTags: true,
};

const NotesContext = createContext({ ...notesInitialState });

const reducer = (state, action) => {
  switch (action.type) {
    case "NOTES_REFRESHED":
      return {
        ...state,
        notes: action.payload,
        needRefresh: false,
      };
    case "SET_TYPE":
      return {
        ...state,
        type: action.payload.type,
        needRefresh: true,
        needRefreshTags: true,
      };
    case "SET_TAG":
      return {
        ...state,
        tag: action.payload.tag,
        needRefresh: true,
      }
    case "TAGS_REFERSHED":
      return {
        ...state,
        tags: action.payload.tags,
        needRefreshTags: false,
      }
    case "NOTES_CHANGED":
      return {
        ...state,
        needRefresh: true,
        needRefreshTags: true,
      };
    default:
      throw new Error("Invalid reducer action type");
  }
};

const NotesProvider = ({ children }) => {
  const [notesState, notesDispatch] = useReducer(reducer, notesInitialState);

  return (
    <NotesContext.Provider value={{ notesState, notesDispatch }}>
      {children}
    </NotesContext.Provider>
  );
};

export { NotesProvider, NotesContext };
