import { createContext, useReducer } from "react";

const userInitialState = {
  uid: undefined,
  displayName: undefined,
};

const UserContext = createContext({ ...userInitialState });

const UserProvider = ({ children }) => {
  const [userState, userDispatch] = useReducer(
    (state, action) => {
      switch (action.type) {
        case "LOGIN":
          return {
            ...state,
            uid: action.payload.user,
            displayName: action.payload.displayName
          }
        case "LOGOUT":
          return {
            ...state,
            uid: null,
            displayName: null,
          }
        case "SET_DISPLAY_NAME":
          return {
            ...state,
            displayName: action.payload.displayName
          }
        default:
          throw new Error("Invalid reducer action type");
      }
    },
    { ...userInitialState }
  );

  return (
    <UserContext.Provider value={{ userState, userDispatch }}>
      {children}
    </UserContext.Provider>
  );
};

export { UserProvider, UserContext };
