import {
  EmailAuthProvider,
  getAuth,
  reauthenticateWithCredential,
  signInWithEmailAndPassword,
  signOut,
  updatePassword,
  updateProfile
} from "@firebase/auth";
import firebase from "./firebase";

export const getUser = async () => {
  const res = await fetch(
    `https://my-software-development.df.r.appspot.com/_api/users/me`,
    {
      headers: {
        Authorization: `Bearer ${await firebase
          .auth()
          .currentUser.getIdToken(/* forceRefresh */ true)}`,
      },
    }
  );
  const json = await res.json();
  return json;
};

export const login = async (username, password) => {
  await signInWithEmailAndPassword(getAuth(firebase), username, password);
};

export const logout = async () => {
  await signOut(getAuth(firebase));
};

export const changeDisplayName = async (displayName) => {
  const user = getAuth(firebase).currentUser
  await updateProfile(user, { displayName: displayName })
}

export const changePassword = async (oldPassword, newPassword) => {
  const user = getAuth(firebase).currentUser
  const credential = EmailAuthProvider.credential(user.email, oldPassword)
  await reauthenticateWithCredential(user, credential)
  await updatePassword(user, newPassword)
}