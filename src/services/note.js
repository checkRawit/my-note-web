import firebase from "./firebase";
import { getAuth, getIdToken } from 'firebase/auth'

const fetchWithAuthorization = async (resource, init = {}) => {
  const authorizationHeader = {
    Authorization: `Bearer ${await getIdToken(getAuth(firebase).currentUser)}`,
  };
  init.headers = init.headers
    ? { ...init.headers, ...authorizationHeader }
    : authorizationHeader;
  return fetch(resource, init);
};

export const fetchNotes = async (type, tag) => {
  const fetchURL = new URL(
    "https://my-software-development.df.r.appspot.com/_api/users/me/notes"
  );

  // Add query parameter
  switch (type) {
    case "DEFAULT":
      fetchURL.searchParams.append("isArchived", false);
      break;
    case "ARCHIVE":
      fetchURL.searchParams.append("isArchived", true);
      break;
    case "ALL":
      break;
    default:
      throw new Error("Unknow isArchived status");
  }

  if (tag != null) {
    fetchURL.searchParams.append("tag", tag)
  }

  const res = await fetchWithAuthorization(fetchURL);
  return await res.json();
};

export const deleteNote = async (id) => {
  const response = await fetchWithAuthorization(
    `https://my-software-development.df.r.appspot.com/_api/notes/${id}`,
    { method: "DELETE" }
  );
  return response.ok;
};

export const postNote = async (message) => {
  const response = await fetchWithAuthorization(
    `https://my-software-development.df.r.appspot.com/_api/notes`,
    {
      method: "POST",
      body: JSON.stringify({
        message: message,
      }),
    }
  );
  return response.ok;
};

export const setNoteArchive = async (noteId, isArchived) => {
  const response = await fetchWithAuthorization(
    `https://my-software-development.df.r.appspot.com/_api/notes/${noteId}`,
    {
      method: "PATCH",
      body: JSON.stringify({
        isArchived,
      }),
    }
  );
  return response.ok;
};

export const editNote = async (noteId, message) => {
  const response = await fetchWithAuthorization(
    `https://my-software-development.df.r.appspot.com/_api/notes/${noteId}`,
    {
      method: "PATCH",
      body: JSON.stringify({
        message,
      }),
    }
  );
  return response.ok;
}

export const fetchNoteTags = async (type) => {
  const fetchURL = new URL(
    "https://my-software-development.df.r.appspot.com/_api/tags"
  );

  // Add query parameter
  switch (type) {
    case "DEFAULT":
      fetchURL.searchParams.append("isArchived", false);
      break;
    case "ARCHIVE":
      fetchURL.searchParams.append("isArchived", true);
      break;
    case "ALL":
      break;
    default:
      throw new Error("Unknow isArchived status");
  }

  const res = await fetchWithAuthorization(fetchURL);
  const json = await res.json();
  return json.tags
}