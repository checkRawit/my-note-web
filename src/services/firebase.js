import { initializeApp } from "firebase/app"

const firebaseApp = initializeApp({
  apiKey: "AIzaSyCDm_hAWKGG4vrgVgSHYWFKTzPB0zx-Apg",
  authDomain: "my-software-development.firebaseapp.com",
  databaseURL: "https://my-software-development.firebaseio.com",
  projectId: "my-software-development",
  storageBucket: "my-software-development.appspot.com",
  messagingSenderId: "134434475588",
  appId: "1:134434475588:web:537d457b338ef49272f611",
  measurementId: "G-J035L1X6ZC",
});

export default firebaseApp;
